module.exports = {
	publicPath: '/klieh.space/',
	pages: {
		index: 'src/main.js',
		404: {
			entry: 'src/error-page.js',
			template: 'public/error-page.html',
			// TODO: only include font and page chunks
		},
	},
};
