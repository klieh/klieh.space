import Vue from 'vue';
import App from './App.vue';
import './assets/scss/vue-command.scss';
import 'typeface-fira-code/index.css';

Vue.config.productionTip = false;

new Vue({
	render: h => h(App)
}).$mount('#app');
