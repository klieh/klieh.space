const store = JSON.parse(localStorage.getItem('exportStore') || '{}');

export function get(key) {
	return store[key];
}

export function getAll() {
	return store;
}

export function set(key, value) {
	store[key] = value;
	localStorage.setItem('exportStore', JSON.stringify(store));
}
