function resolveCommands(list) {
	const commands = {};
	for (const item of list) {
		commands[item] = async () => (await import(`./${item}.vue`)).default;
	}

	return commands;
}

export default {
	echo({ _ }) {
		_.shift();
		return new Option(_.join(' ')).innerHTML;
	},
	...resolveCommands([
		'home',
		'about',
		'stripper',
		'help',
		'export',
		'37c3',
		'matelist',
		'party'
	])
};
